# Script

## Runonce

This script runs all scripts located in a designated directory only once. It can optionally check if a script has been changed using a sha512 checksum.

## installkernel

Used by the kernel to assist with installs and removals.

