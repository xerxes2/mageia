#!/bin/sh

# 0 for no copies/symlinks, 1 for copies, 2 for symlinks
USECOPY=2

# 0 for no initrd, 1 for generating initrd
USEINITRD=1

# 0 for no entry, 1 for adding entry to bootloader
USEENTRY=1

# Options used when generating initrd
INITRDOPTS=""

# Load local settings
if [ -f "/etc/sysconfig/installkernel" ]; then
  . "/etc/sysconfig/installkernel"
fi

# Default action
action="add-kernel"

while getopts R opt; do
  case "${opt}" in
    R) action="remove-kernel";;
    *) ;;
  esac
done
shift $((OPTIND - 1))

version="${1}"
[ -n "${version}" ] || exit 0
[ -z "${DURING_INSTALL}" ] || exit 0

options="--action ${action}"
[ ${USECOPY} != 0 ] || options="--no-short-name ${options}"
[ ${USEINITRD} = 1 ] || options="--no-initrd ${options}"
[ ${USEENTRY} = 1 ] || options="--no-entry ${options}"

if [ -x "/usr/sbin/bootloader-config" ]; then
  bootloader-config --kernel-version "${version}" --initrd-options "${INITRDOPTS}" ${options}
fi

# Set kernel variant
case "${version}" in
  *"desktop"*) variant="desktop";;
  *"server"*) variant="server";;
  *) variant="linus";;
esac

if [ ${action} = "add-kernel" ]; then
  if [ ${USECOPY} = 1 ]; then
     cp -f "/boot/vmlinuz-${version}" "/boot/vmlinuz"
     cp -f "/boot/vmlinuz-${version}" "/boot/vmlinuz-${variant}"
     if [ ${USEINITRD} = 1 ]; then
       cp -f "/boot/initrd-${version}.img" "/boot/initrd.img"
       cp -f "/boot/initrd-${version}.img" "/boot/initrd-${variant}.img"
     fi
  elif [ ${USECOPY} = 2 ]; then
     ln -sf "/boot/vmlinuz-${version}" "/boot/vmlinuz"
     ln -sf "/boot/vmlinuz-${version}" "/boot/vmlinuz-${variant}"
     if [ ${USEINITRD} = 1 ]; then
       ln -sf "/boot/initrd-${version}.img" "/boot/initrd.img"
       ln -sf "/boot/initrd-${version}.img" "/boot/initrd-${variant}.img"
     fi
  fi

