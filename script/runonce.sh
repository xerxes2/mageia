#!/bin/sh

GLOBDIR="/etc/runonce.d"
LOCDIR=".config/runonce.d"
CHECKSUM=true

LOCDIRPATH="${HOME}/${LOCDIR}"

# Create local directory if it does not exists
if [ ! -d "${LOCDIRPATH}" ]; then
  mkdir -p "${LOCDIRPATH}"
fi

for _globpath in "${GLOBDIR}"/*.sh; do
  # Check if global file exists
  if [ -f "${_globpath}" ]; then
    _file="$(basename "${_globpath}")"
    _locpath="${LOCDIRPATH}/${_file}"
    # Check if local file exists
    if [ ! -f "${_locpath}" ]; then
      cp -f "${_globpath}" "${LOCDIRPATH}"
      sh "${_globpath}"
    # Check if file has been changed
    elif ${CHECKSUM}; then
      _globsum="$(sha512sum "${_globpath}" | cut -d ' ' -f 1)"
      _locsum="$(sha512sum "${_locpath}" | cut -d ' ' -f 1)"
      # If file has been changed run again
      if [ ! "${_globsum}" = "${_locsum}" ]; then
        cp -f "${_globpath}" "${LOCDIRPATH}"
        sh "${_globpath}"
      fi
    fi
  fi
done

unset GLOBDIR
unset LOCDIR
unset CHECKSUM
unset LOCDIRPATH
unset _globpath
unset _file
unset _locpath
unset _globsum
unset _locsum

